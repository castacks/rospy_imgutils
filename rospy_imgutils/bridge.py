import cv2
import numpy as np

import cv_bridge
from sensor_msgs.msg import CompressedImage


def imgmsg_to_np(msg):
    """ outputs to mono8 if input is mono8, else bgr """

    buf = np.frombuffer(msg.data, dtype=np.uint8)
    if 'CompressedImage' in str(type(msg)):
        # compressed image will output to bgr8
        img_np = cv2.imdecode(buf, -1)  # LOAD_IMAGE_UNCHANGED
        if 'bayer_rggb8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_BG2BGR)
        elif 'bayer_bggr8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_RG2BGR)
        elif 'bayer_gbrg8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GR2BGR)
        elif 'bayer_grbg8' in msg.format:
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GB2BGR)

    else:
        if msg.encoding == 'mono8':
            img_np = buf.reshape((msg.height, msg.width))
        elif msg.encoding == 'rgb8':
            img_np = buf.reshape((msg.height, msg.width, 3))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_RGB2BGR)
        elif msg.encoding in ('bgr8', '8UC3'):
            img_np = buf.reshape((msg.height, msg.width, 3))
        elif (msg.encoding == 'bayer_rggb8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_BG2BGR)
        elif (msg.encoding == 'bayer_bggr8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_RG2BGR)
        elif (msg.encoding == 'bayer_gbrg8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GR2BGR)
        elif (msg.encoding == 'bayer_grbg8'):
            img_np = buf.reshape((-1, msg.step))
            img_np = cv2.cvtColor(img_np, cv2.COLOR_BAYER_GB2BGR)
        else:
            raise ValueError("can't handle this encoding yet: {}".format(msg.encoding))
    return img_np


def imgmsg_to_np_with_bridge(msg, desired_encoding='passthrough'):
    bridge = cv_bridge.CvBridge()
    return bridge.imgmsg_to_cv2(msg, desired_encoding=desired_encoding)


def np_to_imgmsg(img,
                 stamp=None,
                 frame_id='',
                 compression='none',
                 desired_encoding='passthrough'):
    bridge = cv_bridge.CvBridge()
    if compression not in ('jpg', 'png', 'none'):
        raise ValueError('compression must be jpg/png/none')

    if compression == 'jpg':
        cv_format = '.jpg'
        msg_format = 'jpeg'
    elif compression == 'png':
        cv_format = '.png'
        msg_format = 'png'

    if compression == 'none':
        msg = bridge.cv2_to_imgmsg(img, encoding=desired_encoding)
    else:
        msg = CompressedImage()
        msg.format = msg_format
        ret, buf = cv2.imencode(self.cv_format, img)
        if not ret:
            raise IOError('error encoding image with cv2')
        msg.data = np.asarray(buf, dtype=np.uint8).tostring()
    if stamp is None:
        stamp = rospy.Time.now()
    msg.header.stamp = stamp
    msg.header.frame_id = frame_id
    return msg
