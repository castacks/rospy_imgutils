# rospy_imgutils

Daniel Maturana (dimatura@gmail.com

Python utilities for images in ROS.

## License

BSD, see LICENSE.
